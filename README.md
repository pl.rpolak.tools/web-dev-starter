# Web Dev Starter

Starter project for dev development with auto reload.

Prerequisites:

- Installed NodeJS

Installation:

- Run <code>npm install</code>

Run:

- Run <code>npm run dev</code> to run project,
website will be available on <code>localhost:3000</code>
